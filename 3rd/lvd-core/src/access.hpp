/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <utility>

// ----------

namespace lvd {
namespace __impl__ {

template <typename Object, typename Object::Type type>
class Accessor {
 public:
  friend typename Object::Type access(Object) {
    return type;
  }
};

template <typename Object, typename Thingy>
class Accessee {
 public:
  using  Type      = Thingy;
  friend Type access(Object);
};

template <class C, class T>
class NormalMemberT {
 public:
  using Type = T  C::*;
};

template <         class T>
class StaticMemberT {
 public:
  using Type = T      ;
};

template <class C, class R, class... Args>
class NormalMethodT {
 public:
  using Type = R (C::*)(Args...);
};

template <         class R, class... Args>
class StaticMethodT {
 public:
  using Type = R       (Args...);
};

template <class C, class R, class... Args>
class NormalStableT {
 public:
  using Type = R (C::*)(Args...) const;
};

}  // namespace __impl__

// ----------

template <class Signature, class Object>
auto& access_normal_member(Object& object) {
  return  object.*access(Signature());
}

template <class Signature              >
auto& access_static_member(              ) {
  return         *access(Signature());
}

template <class Signature, class Object, class... Args>
auto  access_normal_method(Object& object, Args&&... args) {
  return (object.*access(Signature()))(std::forward<Args>(args)...);
}

template <class Signature,               class... Args>
auto  access_static_method(                Args&&... args) {
  return (       *access(Signature()))(std::forward<Args>(args)...);
}

// ----------

#define LVD_ACCESS_NORMAL_MEMBER(Name, Object, Member, ...) \
  class Name : public lvd::__impl__::Accessee<Name, lvd::__impl__::NormalMemberT<Object, __VA_ARGS__>::Type > {}; \
  template class lvd::__impl__::Accessor<Name, &Object::Member>;

#define LVD_ACCESS_STATIC_MEMBER(Name, Object, Member, ...) \
  class Name : public lvd::__impl__::Accessee<Name, lvd::__impl__::StaticMemberT<        __VA_ARGS__>::Type*> {}; \
  template class lvd::__impl__::Accessor<Name, &Object::Member>;

#define LVD_ACCESS_NORMAL_METHOD_HELPER(Object, Return, ...) Return (Object::*)(__VA_ARGS__)
#define LVD_ACCESS_NORMAL_METHOD(Name, Object, Member, ...) \
  class Name : public lvd::__impl__::Accessee<Name, lvd::__impl__::NormalMethodT<Object, __VA_ARGS__>::Type > {}; \
  template class lvd::__impl__::Accessor<Name, static_cast<LVD_ACCESS_NORMAL_METHOD_HELPER(Object, __VA_ARGS__)>(&Object::Member)>;

#define LVD_ACCESS_STATIC_METHOD_HELPER(        Return, ...) Return (        *)(__VA_ARGS__)
#define LVD_ACCESS_STATIC_METHOD(Name, Object, Member, ...) \
  class Name : public lvd::__impl__::Accessee<Name, lvd::__impl__::StaticMethodT<        __VA_ARGS__>::Type*> {}; \
  template class lvd::__impl__::Accessor<Name, static_cast<LVD_ACCESS_STATIC_METHOD_HELPER(        __VA_ARGS__)>(&Object::Member)>;

#define LVD_ACCESS_NORMAL_STABLE_HELPER(Object, Return, ...) Return (Object::*)(__VA_ARGS__) const
#define LVD_ACCESS_NORMAL_STABLE(Name, Object, Member, ...) \
  class Name : public lvd::__impl__::Accessee<Name, lvd::__impl__::NormalStableT<Object, __VA_ARGS__>::Type > {}; \
  template class lvd::__impl__::Accessor<Name, static_cast<LVD_ACCESS_NORMAL_STABLE_HELPER(Object, __VA_ARGS__)>(&Object::Member)>;

}  // namespace lvd
