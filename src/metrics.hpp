/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QString>

#include <Cutelyst/Context>
#include <Cutelyst/Controller>

#include "lvd/logger.hpp"

// ----------

namespace lvd::temperedtheus {

class Metrics : public Cutelyst::Controller {
  Q_OBJECT LVD_LOGGER

  C_NAMESPACE("metrics")

 public:
  Metrics(QObject* parent = nullptr);
  ~Metrics() override;

 public:
  C_ATTR(metrics, :Path :AutoArgs)
  void metrics(Cutelyst::Context* context);
};

}  // namespace lvd::temperedtheus
