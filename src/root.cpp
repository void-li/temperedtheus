/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "root.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QByteArray>

#include <Cutelyst/Response>

#include "lvd/shield.hpp"

// ----------

namespace lvd::temperedtheus {

Root::Root(QObject* parent)
    : Cutelyst::Controller(parent) {
  LVD_LOG_T();
}

Root::~Root() {
  LVD_LOG_T();
}

// ----------

void Root::root(Cutelyst::Context* context) {
  LVD_LOG_T();
  LVD_SHIELD;

  context->response()->setStatus(Cutelyst::Response::NotFound);
  context->response()->body() = " ";

  LVD_SHIELD_END;
}

// ----------

void Root::End (Cutelyst::Context* context) {
  LVD_LOG_T();
  LVD_SHIELD;

  Q_UNUSED(context)

  LVD_SHIELD_END;
}

}  // namespace lvd::temperedtheus
