/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "main.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDebug>
#include <QList>
#include <QStringList>

#include <Cutelyst/Plugins/View/Grantlee/grantleeview.h>

#include "lvd/logger.hpp"
#include "lvd/metatype.hpp"
#include "lvd/shield.hpp"

#include "config.hpp"
#include "metrics.hpp"
#include "root.hpp"

// ----------

namespace lvd::temperedtheus {

Main::Main(QObject* parent)
    : Cutelyst::Application(parent) {}

Main::~Main() {
  lvd::Logger::restore();
}

// ----------

bool Main::init() {
  return
  LVD_SHIELD;

  lvd::Metatype::metatype();

  QCoreApplication::setApplicationName (lvd::temperedtheus::config::App_Name());
  QCoreApplication::setApplicationVersion(lvd::temperedtheus::config::App_Vers());

  QCoreApplication::setOrganizationName(lvd::temperedtheus::config::Org_Name());
  QCoreApplication::setOrganizationDomain(lvd::temperedtheus::config::Org_Addr());

  // ----------

  lvd::Logger::install();
  LVD_LOGFUN

  // ----------

  QCommandLineParser qcommandlineparser;
  lvd::Logger::setup_arguments(qcommandlineparser);

  QStringList arguments = QCoreApplication::arguments();

  int arguments_index = arguments.lastIndexOf("--");
  if (arguments_index > -1) {
    arguments = arguments.mid(0, 1) + arguments.mid(arguments_index + 1);
  }

  qcommandlineparser.parse(arguments);
  lvd::Logger::parse_arguments(qcommandlineparser);

  LVD_LOG_I() >> &QDebug::noquote
              << QCoreApplication::applicationName   ()
              << QCoreApplication::applicationVersion();

  // ----------

  //! /
  new Root   (this);

  //! /metrics
  new Metrics(this);

  // ----------

  new Cutelyst::GrantleeView(this);

  return true;
  LVD_SHIELD_END;
}

}  // namespace lvd::temperedtheus
