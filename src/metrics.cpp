/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "metrics.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <cstdlib>

#include <tempered-util.h>
#include <tempered.h>

#include <QByteArray>
#include <QSettings>
#include <QStringList>
#include <QVariant>
#include <QVariantHash>
#include <QVariantList>

#include <Cutelyst/Response>

#include "lvd/settings.hpp"
#include "lvd/shield.hpp"

// ----------

namespace lvd::temperedtheus {

Metrics::Metrics(QObject* parent)
    : Cutelyst::Controller(parent) {
  LVD_LOG_T();
}

Metrics::~Metrics() {
  LVD_LOG_T();
}

// ----------

void Metrics::metrics(Cutelyst::Context* context) {
  LVD_LOG_T();
  LVD_SHIELD;

  char* error = nullptr;
  bool  ok;

  ok = tempered_init(&error);
  if (!ok) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << error;

    context->response()->setStatus(Cutelyst::Response::InternalServerError);
    context->response()->setBody(message);
    return;
  }

  LVD_FINALLY {
    ok = tempered_exit(&error);
    if (!ok) {
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << error;

      context->response()->setStatus(Cutelyst::Response::InternalServerError);
      context->response()->setBody(message);
      return;
    }
  };

  tempered_device_list* device_list = tempered_enumerate(&error);
  if (!device_list && error) {
    LVD_LOGBUF message;
    LVD_LOG_W(&message) << error;

    context->response()->setStatus(Cutelyst::Response::InternalServerError);
    context->response()->setBody(message);
    return;
  }

  LVD_FINALLY {
    tempered_free_device_list(device_list);
  };

  Settings settings(QSettings::SystemScope);

  QVariantList sensors;
   QStringList oopsies;

  for (tempered_device_list* device_item = device_list;
                             device_item;
                             device_item = device_item->next) {
    QVariantHash sensor;

    error = nullptr;

    QString veid = QString::number(device_item-> vendor_id, 0x10);
    sensor.insert("veid", veid);

    QString prid = QString::number(device_item->product_id, 0x10);
    sensor.insert("prid", prid);

    QString path = QString::fromLocal8Bit(device_item->     path);
    sensor.insert("path", path);

    QString name = QString::fromLocal8Bit(device_item->type_name);
    sensor.insert("name", name);

    tempered_device* device = tempered_open(device_item, &error);
    if (!device) {
      if (error) {
        LVD_LOGBUF message;
        LVD_LOG_W(&message) << error;

        oopsies << message;
      }

      continue;
    }

    LVD_FINALLY {
      tempered_close(device);
    };

    ok = tempered_read_sensors(device);
    if (!ok) {
      if (error) {
        LVD_LOGBUF message;
        LVD_LOG_W(&message) << error;

        oopsies << message;
      }

      continue;
    }

    const int sensor_count = tempered_get_sensor_count(device);
    for (int sensor_index = 0; sensor_index < sensor_count; sensor_index ++) {
      int type = tempered_get_sensor_type(device, sensor_index);
      if (type & TEMPERED_SENSOR_TYPE_TEMPERATURE) {
        float temf;

        ok = tempered_get_temperature(device, sensor_index, &temf);
        if (!ok) {
          if (error) {
            LVD_LOGBUF message;
            LVD_LOG_W(&message) << error;

            oopsies << message;
          }

          continue;
        }

        QString calibration;

        if        (settings.              contains(path)) {
          calibration = settings.value(path                 ).toString();
        } else if (settings.childGroups().contains(path)) {
          calibration = settings.value(path + "/calibration").toString();
        }

        if (!calibration.isEmpty()) {
          QByteArray factor_descr = calibration.toLocal8Bit();
          int        factor_count;

          float* factors = tempered_util__parse_calibration_string(
              factor_descr.data(), &factor_count, false);

          LVD_FINALLY { free(factors); };

          if (factors) {
            temf = tempered_util__calibrate_value(temf, factor_count, factors);
          }
        }

        const double temp = static_cast<double>(temf);
        sensor.insert("temp", QString::number(temp));
      }

      if (sensor.contains("temp")) {
        sensors.append(sensor);
      }
    }
  }

  context->response()->setContentType("text/plain");

  context->setStash("sensors", sensors);
  context->setStash("oopsies", oopsies);

  LVD_SHIELD_END;
}

}  // namespace lvd::temperedtheus
